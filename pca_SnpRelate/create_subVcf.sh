set -x

############################Preparing VCF for all ccessions (including al available Azores)################################
#population identifier
pop_id=all

#path to pop_file (i have picked the best 20 individuals per cluster based on missing data proportions, excluding ref because of non-consistent formatting in the mask file)
path2popfile=strains_after_kinship_filtering	

#path to the large snp table (assumes plink2 extensions
path2snps=/Volumes/netscratch/dep_tsiantis/common/bjorn/genomic_data/BS_MAFFT_2018-06-06/plink2_files/masked/snp.masked

#path to the bed file with locations of exons in the Chr 1-8
#path2exons=/netscratch/dep_tsiantis/grp_laurent/laurent/cardamine/genome_paper/data/annotation/carhr38_exons1_onlyChr1_to_8_recodeChr.bed
#path to the bed file with locations of regions to exclude in Chr 1-8 (exons or low-rec pericentromeric)
path2exons=/Volumes/netscratch/dep_tsiantis/grp_laurent/laurent/cardamine/demography/iberia_euronorth/data_local/mask_construction/input/regions_to_be_excluded_exons_low_rec.txt

#start by extracting ids of SNPs in chr 1 and sub-sample (--geno should be removed for detection of best sub-sample)
#no filtering on missing data
plink2 --pfile $path2snps vzs --export vcf id-paste=iid --keep $path2popfile --maf 0.05 --max-maf 0.9999 --ref-from-fa  /Volumes/netscratch/dep_tsiantis/grp_laurent/laurent/cardamine/genome_paper/data/reference/chi_v1.fa --out ${pop_id}

#modifying version and massaging data
head -n 1 ${pop_id}.vcf  | sed 's/v4.3/v4.2/' > header
cat ${pop_id}.vcf | sed '1d' > temp 
cat header temp > ${pop_id}.vcf
rm header temp 
#bgzip ${pop_id}.vcf



############################Preparing VCF for set of lines used in Pop-Genome manuscript ################################

#population identifier
pop_id=set3

#path to pop_file (i have picked the best 20 individuals per cluster based on missing data proportions, excluding ref because of non-consistent formatting in the mask file)
path2popfile=strains.to.keep.3	

#path to the large snp table (assumes plink2 extensions
path2snps=/Volumes/netscratch/dep_tsiantis/common/bjorn/genomic_data/BS_MAFFT_2018-06-06/plink2_files/masked/snp.masked

#path to the bed file with locations of exons in the Chr 1-8
#path2exons=/netscratch/dep_tsiantis/grp_laurent/laurent/cardamine/genome_paper/data/annotation/carhr38_exons1_onlyChr1_to_8_recodeChr.bed
#path to the bed file with locations of regions to exclude in Chr 1-8 (exons or low-rec pericentromeric)
path2exons=/Volumes/netscratch/dep_tsiantis/grp_laurent/laurent/cardamine/demography/iberia_euronorth/data_local/mask_construction/input/regions_to_be_excluded_exons_low_rec.txt

#start by extracting ids of SNPs in chr 1 and sub-sample (--geno should be removed for detection of best sub-sample)
#no filtering on missing data
plink2 --pfile $path2snps vzs --export vcf id-paste=iid --keep $path2popfile --maf 0.05 --max-maf 0.9999 --fa  /Volumes/netscratch/dep_tsiantis/grp_laurent/laurent/cardamine/genome_paper/data/reference/chi_v1.fa --out ${pop_id}

#modifying version and massaging data
head -n 1 ${pop_id}.vcf  | sed 's/v4.3/v4.2/' > header
cat ${pop_id}.vcf | sed '1d' > temp 
cat header temp > ${pop_id}.vcf
rm header temp 
#bgzip ${pop_id}.vcf